library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity DCC_bit_TB is
end DCC_bit_TB;

architecture RTL of DCC_bit_TB is

-- Paramètres de configuration
constant counter_value0 : natural := 100;
constant counter_value1 : natural := 58;

--Inputs
signal clk100, clk1, reset_n : std_logic := '0';
signal go_0 : std_logic := '0';
signal go_1 : std_logic := '0';

--Outputs
signal fin_0 : std_logic;
signal fin_1 : std_logic;

begin

uut1 : entity work.DCC_bit -- Simulation du générateur bit 0 format DCC 
       generic map
       (
         counter_value => counter_value0
       )
       port map
       (
         clk100  => clk100,
         clk1    => clk1,
         reset_n => reset_n,
         go      => go_0,
         fin     => fin_0
       );

uut2 : entity work.DCC_bit -- Simulation du générateur bit 0 format DCC 
       generic map
       (
         counter_value => counter_value1
       )
       port map
       (
         clk100  => clk100,
         clk1    => clk1,
         reset_n => reset_n,
         go      => go_1,
         fin     => fin_1
       );

clk100 <= not clk100 after 5 ns;
clk1 <= not clk1 after 500 ns;
reset_n <= '1' after 10 ns;

go_0 <= '1' after 50 ns, '0' after 60 ns;
go_1 <= '1' after 50 ns, '0' after 60 ns;

end RTL;