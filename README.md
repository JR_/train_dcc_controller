.
├── README.md
├── src_c
│   └── main.c
├── src_vhd
│   ├── Simulations
│   │   ├── DCC_bit_TB.vhd
│   │   ├── DCC_clkdiv_TB.vhd
│   │   ├── DCC_frame_generator_TB.vhd
│   │   ├── DCC_FSM_TB.vhd
│   │   ├── DCC_register_TB.vhd
│   │   └── DCC_Top_TB.vhd
│   └── Sources
│       ├── DCC_bit.vhd
│       ├── DCC_clkdiv.vhd
│       ├── DCC_frame_generator.vhd
│       ├── DCC_FSM.vhd
│       ├── DCC_register.vhd
│       ├── DCC_Top.vhd
└── src_xdc
    ├── Nexys4DDR_Pinmode_Codesign_Hard_Soft.xdc
    └── Nexys4DDR_Pinmode_FullHard.xdc

269 directories, 1159 files
