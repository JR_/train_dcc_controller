library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity DCC_FSM_TB is
end DCC_FSM_TB;

architecture RTL of DCC_FSM_TB is

--Inputs
signal clk100, clk1, reset_n : std_logic := '0';
signal serial_frame : std_logic := '0';
signal last_bit_frame : std_logic := '0';
signal fin_0, fin_1 : std_logic := '0';

--Outputs
signal command : std_logic_vector(1 downto 0);
signal go_0, go_1 : std_logic;

begin

uut : entity work.DCC_FSM
      port map
      (
        clk100         => clk100,
        clk1           => clk1,
        reset_n        => reset_n,
        serial_frame   => serial_frame,
        last_bit_frame => last_bit_frame,
        fin_0          => fin_0,
        fin_1          => fin_1,
        command        => command,
        go_0           => go_0,
        go_1           => go_1
      );

clk100 <= not clk100 after 1 ps;
clk1 <= not clk1 after 100 ps; 
reset_n <= '1' after 10 ps;

serial_frame   <= '1' after 2000001 ps;
last_bit_frame <= '1' after 2000001 ps;

fin_1 <= '1' after 4 us, '0' after 4000002 ps;
fin_0 <= '1' after 2 us, '0' after 2000002 ps;

end RTL;
