library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity DCC_Top_TB is
end DCC_Top_TB;

architecture RTL of DCC_Top_TB is

signal clk100, reset_n : std_logic := '0';
signal switches_board : std_logic_vector(11 downto 0) := (others => '0');
signal buttons_board : std_logic := '0';

--Outputs
signal LEDs : std_logic_vector(11 downto 0);
signal out_DCC : std_logic;

begin

uut : entity work.DCC_Top
      port map
      (
        clk100         => clk100,
        reset_n        => reset_n,
        buttons_board  => buttons_board,
        switches_board => switches_board,
        LEDs           => LEDs,
        out_DCC        => out_DCC
      );

clk100 <= not clk100 after 5 ns;
reset_n <= '1' after 10 ns;

buttons_board <= not buttons_board after 500 ns, '0' after 10 ms;

switches_board (1 downto 0) <= "01";
switches_board (9 downto 2) <= x"AB";

end RTL;