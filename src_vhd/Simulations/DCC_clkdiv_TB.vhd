library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity DCC_clkdiv_TB is
end DCC_clkdiv_TB;

architecture RTL of DCC_clkdiv_TB is

-- Paramètres de configuration
constant counter_value : natural := 100;

--Inputs
signal clk_in, reset_n : std_logic := '0';

--Outputs
signal clk_out : std_logic;

begin

uut : entity work.DCC_clkdiv
      generic map
      (
        counter_value => counter_value
      )
      port map
      (
        clk_in  => clk_in,
        reset_n => reset_n,
        clk_out => clk_out
      );

clk_in <= not clk_in after 1 ps;
reset_n <= '1' after 10 ps;

end RTL;