library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity DCC_Top is port
          (
		       -- Inputs
		        clk100, reset_n : in std_logic;
		        
		        buttons_board  : in std_logic;                     -- Permet de valider d'une trame
            switches_board : in std_logic_vector(11 downto 0); -- Permet de constituer une trame
           
           -- Outputs
		        LEDs    : out std_logic_vector(11 downto 0);       -- Permet de visualiser la derni�re trame envoy�e
		        out_DCC : out std_logic                            -- Permet de r�cup�rer la trame en format DCC en liaison s�rie
          );
end DCC_Top;

architecture RTL of DCC_Top is

-- Param�tres de configuration
constant clk_factor_division : natural := 100; -- Param�tre g�rant le facteur de division d'horloge 100 MHz -> 1 MHz
constant counter_DCC_0       : natural := 100; -- Param�tre g�rant le nombre de cycles � compter pour le g�n�rateur de bit 0 format DCC (1 cycles = 1 �s)
constant counter_DCC_1       : natural := 58;  -- Param�tre g�rant le nombre de cycles � compter pour le g�n�rateur de bit 1 format DCC (1 cycles = 1 �s)

signal clk1 : std_logic; -- Horloge 1 MHz

-- Signaux entre l'interface utilisateur le g�n�rateur de trame
signal enable_frame    : std_logic;
signal function_type   : std_logic_vector(1 downto 0);
signal function_select : std_logic_vector(7 downto 0);
signal train_addr      : std_logic_vector(1 downto 0);

-- Signaux entre le g�n�rateur de trame et le registre DCC
signal parallel_frame : std_logic_vector(50 downto 0); 

-- Signaux entre le registre DCC et la machine � �tats
signal serial_frame   : std_logic;
signal last_bit_frame : std_logic;
signal command        : std_logic_vector(1 downto 0);

-- Signaux entre la machine � �tats et les g�n�rateurs de bits format DCC
signal go_0  : std_logic;
signal go_1  : std_logic;
signal fin_0 : std_logic;
signal fin_1 : std_logic;
signal DCC_0 : std_logic;
signal DCC_1 : std_logic;

begin

-- Instanciation du module clkdiv (module permettant de r�duire l'horloge)
U1 : entity work.DCC_clkdiv
     generic map
     (
       counter_value => clk_factor_division
     )
     port map
     (
       clk_in  => clk100,
       reset_n => reset_n,
       clk_out => clk1
     );

-- Routage de l'interface utilisateur
enable_frame <= buttons_board;
function_type <= switches_board(1 downto 0);
function_select <= switches_board(9 downto 2);
train_addr <= switches_board(11 downto 10);
         
-- Instanciation du module DCC_trame_generator (module permettant de g�n�rer une trame utilisateur)
U2 : entity work.DCC_frame_generator
     port map
     (
       clk100          => clk100,
       reset_n         => reset_n,
       enable_frame    => enable_frame,
       function_type   => function_type,
       function_select => function_select,
       train_addr      => train_addr,
       parallel_frame  => parallel_frame,
       LEDs            => LEDs
     );

-- Instanciation du module DCC_register (registre permettant de "s�rialiser" une trame utilisateur)
U3 : entity work.DCC_register
     port map
     (
       clk100         => clk100,
       reset_n        => reset_n,
       command        => command,
       parallel_frame => parallel_frame, -- Big endian
       serial_frame   => serial_frame,
       last_bit_frame => last_bit_frame 
     );

-- Instanciation du module DCC_FSM (module permettant de g�n�rer une trame en protocole DCC)
U4 : entity work.DCC_FSM
     port map
     (
       clk100         => clk100,
       reset_n        => reset_n,
       clk1           => clk1,
       serial_frame   => serial_frame,
       last_bit_frame => last_bit_frame ,
       fin_0          => fin_0,            
       fin_1          => fin_1,
       command        => command,
       go_0           => go_0,
       go_1           => go_1
     );

-- Instanciation du module DCC_bit_zero (module permettant de g�n�rer un 0 en protocole DCC)
U5 : entity work.DCC_bit
     generic map
     (
       counter_value => counter_DCC_0
     )
     port map
     (
       clk100  => clk100,
       reset_n => reset_n,
       clk1    => clk1,
       go      => go_0,
       fin     => fin_0,
       DCC     => DCC_0
     );

-- Instanciation du module DCC_bit_one (module permettant de g�n�rer un 1 en protocole DCC)
U6 : entity work.DCC_bit
     generic map
     (
       counter_value => counter_DCC_1
     )
     port map
     (
       clk100  => clk100,
       reset_n => reset_n,
       clk1    => clk1,
       go      => go_1,
       fin     => fin_1,
       DCC     => DCC_1
     );

--Routage vers la sortie fournissant la trame format DCC
out_DCC <= DCC_0 or DCC_1;

end RTL;
