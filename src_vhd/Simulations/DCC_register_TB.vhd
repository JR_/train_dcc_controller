library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity DCC_register_TB is
end DCC_register_TB;

architecture RTL of DCC_register_TB is

--Inputs
signal clk100, reset_n : std_logic := '0';
signal command : std_logic_vector(1 downto 0) := (others => '0');
signal parallel_frame : std_logic_vector(50 downto 0) := (others => '0');

--Outputs
signal serial_frame : std_logic;
signal last_bit_frame : std_logic;

begin

uut : entity work.DCC_register
      port map
      (
        clk100         => clk100,
        reset_n        => reset_n,
        command        => command,
        parallel_frame => parallel_frame,
        serial_frame   => serial_frame,
        last_bit_frame => last_bit_frame
      );

clk100 <= not clk100 after 1 ps;
reset_n <= '1' after 10 ps;

command <= "01" after 20 ps, "10" after 30 ps, "01" after 220 ps, "10" after 230 ps; 

parallel_frame <= x"FFFFEEEEDDDD"&"101" after 20 ps, x"BBBBAAAA9999"&"010" after 40 ps;

end RTL;