library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity DCC_frame_generator_TB is
end DCC_frame_generator_TB;

architecture RTL of DCC_frame_generator_TB is

--Inputs
signal clk100, reset_n : std_logic := '0';
signal enable_frame : std_logic := '0';
signal function_type : std_logic_vector(1 downto 0) := (others => '0');
signal function_select : std_logic_vector(7 downto 0) := (others => '0');
signal train_addr : std_logic_vector(1 downto 0) := (others => '0');

--Outputs
signal parallel_frame : std_logic_vector(50 downto 0);
signal LEDs : std_logic_vector(11 downto 0);

begin

uut : entity work.DCC_frame_generator
      port map
      (
        clk100          => clk100,
        reset_n         => reset_n,
        enable_frame    => enable_frame,
        function_type   => function_type,
        function_select => function_select,
        train_addr      => train_addr,
        parallel_frame  => parallel_frame,
        LEDs            => LEDs
      );

clk100 <= not clk100 after 1 ps;
reset_n <= '1' after 10 ps;

enable_frame <= not enable_frame after 15 ps;

p_function_type : process --Process g�rant la variation du type de fonction
  variable temp : std_logic_vector(1 downto 0) := "00";        
  begin
 	  temp := temp+1;
 	  function_type <= temp;
	  wait for 20 ps;
  end process p_function_type; 

p_function_select : process --Process g�rant les valeurs sur les boutons poussoirs
  variable temp : std_logic_vector(7 downto 0) := (others => '0');        
  begin
 	  temp := temp+1;
 	  function_select <= temp;
	  wait for 10 ps;
end process p_function_select; 

end RTL;