library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;

entity DCC_clkdiv is 
          generic
          (
            -- Param�tres de configuration
            counter_value : natural         -- Param�tre fixant le facteur de division de l'horloge (doit �tre positif et pair)
          );
          port
          (
            -- Inputs
            clk_in, reset_n : in std_logic; -- Entr�e de l'horloge
           
            -- Output
            clk_out : out std_logic         -- Sortie de l'horloge
          );
end DCC_clkdiv;

architecture RTL of DCC_clkdiv is

-- Configuration (auto)
constant nb_bits   : integer := integer(ceil(log2(real(counter_value))))+1;                                                -- Nombre de bits du compteur
constant nb_cycles : std_logic_vector(nb_bits-1 downto 0):= std_logic_vector(to_unsigned((counter_value/2)-1, nb_bits)); -- Nombre de cycle � compter (demi p�riode)


signal s_clk_out : std_logic:='0';                                         -- Connecteur � la sortie clk_out
signal compteur : std_logic_vector(nb_bits-1 downto 0) := (others => '0'); -- Compteur 

begin

p_clk : process(clk_in, reset_n)
  begin
    if reset_n = '0' then             -- Reset asynchrone actif sur niveau bas
      s_clk_out <= '0';
    elsif rising_edge(clk_in) then    -- Compteur actif sur front montant
      if (compteur = nb_cycles) then  -- Inversion du signal si demi-p�riode atteinte
        s_clk_out <= not(s_clk_out);
        compteur <= (others => '0');  
      else
        compteur <= compteur + 1;
      end if;
    end if;
end process p_clk;

--Routage vers la sortie
clk_out <= s_clk_out;

end RTL;
