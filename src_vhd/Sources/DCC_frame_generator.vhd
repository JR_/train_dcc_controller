library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity DCC_frame_generator is port
          (
		        -- Inputs
            clk100, reset_n : in std_logic;
            
            enable_frame    : in std_logic;                     -- Permet de charger une trame
            function_type   : in std_logic_vector(1 downto 0);  -- Permet de selectionner le groupe de fonction
            function_select : in std_logic_vector(7 downto 0);  -- Permet de selectionner les sous-fonctions du groupe de fonction s�lectionn� pr�c�demment            train_addr : in std_logic_vector(1 downto 0;
            train_addr      : in std_logic_vector(1 downto 0);  -- Permet de s�lectionner le train dont on souhaite contr�ler
           
            -- Outputs
		        parallel_frame : out std_logic_vector(50 downto 0); -- Permet de r�cup�rer la trame de fa�on parall�le
		        LEDs           : out std_logic_vector(11 downto 0)  -- Permet de visualiser la derni�re trame envoy�e
          );
end DCC_frame_generator;

architecture RTL of DCC_frame_generator is

-- Adresses des trains contr�lables
constant train_addr1 : std_logic_vector(7 downto 0) := x"01";
constant train_addr2 : std_logic_vector(7 downto 0) := x"02";
constant train_addr3 : std_logic_vector(7 downto 0) := x"03";
constant train_addr4 : std_logic_vector(7 downto 0) := x"04";

-- Signaux de commande de la vitesse
signal byte_speed : std_logic_vector(7 downto 0);
signal speed_enable : std_logic;

signal train_speed_level : std_logic_vector(4 downto 0);
signal train_direction : std_logic;

-- Signaux de commmande groupe 1
signal functions1 : std_logic_vector(4 downto 0);
signal byte_function1 : std_logic_vector(7 downto 0);
signal functions1_enable : std_logic;

signal light : std_logic;                    -- F0
signal sound : std_logic;                    -- F1
signal COR_FR1 : std_logic;                  -- F2
signal COR_FR2 : std_logic;                  -- F3
signal turbo : std_logic;                    -- F4

-- Signaux de commmande groupe 2
signal functions2 : std_logic_vector(7 downto 0);
signal byte_function2 : std_logic_vector(7 downto 0);
signal functions2_enable : std_logic;

-- Prioritaire
signal compressor : std_logic;               -- F5
signal acceleration : std_logic;             -- F6
signal curve_creaking : std_logic;           -- F7
signal clank : std_logic;                    -- F8

-- Non prioritaire
signal ventilator : std_logic;               -- F9
signal conductor_signal : std_logic;         -- F10
signal short_COR_FR1 : std_logic;            -- F11
signal short_COR_FR2 : std_logic;            -- F12

signal S : std_logic; -- Indique s'il s'agit des fonctions F5 � F8 (�tat bas) ou de F9 � F12 (�tat haut) 

-- Signaux de commmande groupe 3
signal functions3 : std_logic_vector(7 downto 0);
signal byte_function3 : std_logic_vector(7 downto 0);
signal functions3_enable : std_logic;

signal FR1_station_announcement : std_logic; -- F13
signal FR2_station_announcement : std_logic; -- F14
signal FR1_alert_signal : std_logic;         -- F15
signal FR2_alert_signal : std_logic;         -- F16
signal door : std_logic;                     -- F17
signal valve : std_logic;                    -- F18
signal coupling : std_logic;                 -- F19
signal sand : std_logic;                     -- F20

-- Signaux pour la constitution de trame
signal byte_address : std_logic_vector(7 downto 0);
signal byte1_command : std_logic_vector(7 downto 0);
constant byte2_command : std_logic_vector(7 downto 0) := "11011110";
signal byte_XOR : std_logic_vector(7 downto 0);
signal full_frame : std_logic_vector(50 downto 0); -- Correspond � la "FIFO"

-- Signaux pour l'affichage sur les LEDs de la derni�re trame constitu�e
signal s_LEDs : std_logic_vector(11 downto 0);

-- Indique si le champ d'adresse des fonctions sera sur 1 (�tat bas) ou 2 octets (�tat haut)
signal size_frame : std_logic;

begin
 
-- Combinatoire des commandes groupes 1
functions1        <= function_select(4 downto 0);
functions1_enable <= '1' when function_type = "01" else '0';

light   <= '1' when functions1(0) = '1' else '0';
sound   <= '1' when functions1(1) = '1' else '0';
COR_FR1 <= '1' when functions1(2) = '1' else '0';
COR_FR2 <= '1' when functions1(3) = '1' else '0';
turbo   <= '1' when functions1(4) = '1' else '0';

byte_function1 <= "100"&light&turbo&COR_FR2&COR_FR1&sound;

-- Combinatoire des commandes groupes 2
functions2        <= function_select(7 downto 0);
functions2_enable <= '1' when function_type = "10" else '0';

-- S = 0
compressor       <= '1' when functions2(0) = '1' else '0';
acceleration     <= '1' when functions2(1) = '1' else '0';
curve_creaking   <= '1' when functions2(2) = '1' else '0';
clank            <= '1' when functions2(3) = '1' else '0';

-- S = 1
ventilator       <= '1' when functions2(4) = '1' else '0';
conductor_signal <= '1' when functions2(5) = '1' else '0';
short_COR_FR1    <= '1' when functions2(6) = '1' else '0';
short_COR_FR2    <= '1' when functions2(7) = '1' else '0';

S <= '1' when (compressor or acceleration or curve_creaking or clank) = '1' else '0';

byte_function2 <= "101"&'1'&clank&curve_creaking&acceleration&compressor when (functions2_enable = '1' and S = '1')
             else "101"&'0'&short_COR_FR2&short_COR_FR1&conductor_signal&ventilator;

-- Combinatoire des commandes groupes 3
functions3        <= function_select(7 downto 0);
functions3_enable <= '1' when function_type = "11" else '0';

FR1_station_announcement <= '1' when functions3(0) = '1' else '0';
FR2_station_announcement <= '1' when functions3(1) = '1' else '0';
FR1_alert_signal         <= '1' when functions3(2) = '1' else '0';
FR2_alert_signal         <= '1' when functions3(3) = '1' else '0';
door                     <= '1' when functions3(4) = '1' else '0';
valve                    <= '1' when functions3(5) = '1' else '0';
coupling                 <= '1' when functions3(6) = '1' else '0';
sand                     <= '1' when functions3(7) = '1' else '0';

byte_function3 <= sand&coupling&valve&door&FR2_alert_signal&FR1_alert_signal&FR2_station_announcement&FR1_station_announcement;

-- Combinatoire g�rant la vitesse et la direction des trains
speed_enable <= '1' when function_type = "00" else '0';

train_direction   <= function_select(0);
train_speed_level <= function_select(5 downto 1);
byte_speed        <= '0'&'1'&train_direction&train_speed_level;

-- Combinatoire de l'octet du champ d'adresse des trains
byte_address <= train_addr1 when train_addr = "00"
           else train_addr2 when train_addr = "01"
           else train_addr3 when train_addr = "10"            
           else train_addr4;

-- Combinatoire de l'octet du champ de commande 1
byte1_command <= byte_function1 when functions1_enable = '1'
            else byte_function2 when functions2_enable = '1'
            else byte_function3 when functions3_enable = '1'
            else byte_speed;

-- Combinatoire de l'octet du contr�le XOR
byte_XOR <= byte1_command xor byte_address when size_frame = '0'
       else byte1_command xor byte2_command xor byte_address;

-- Combinatoire de construction compl�te de la trame
size_frame <= '1' when functions3_enable = '1' else '0';

full_frame <= "111"&x"FFFFF"&'0'&byte_address&'0'&byte1_command&'0'&byte_XOR&'1' when size_frame = '0'
         else "11"&x"FFF"&'0'&byte_address&'0'&byte2_command&'0'&byte1_command&'0'&byte_XOR&'1';

-- Combinatoire et process g�rant l'envoi vers les sorties de la trame
p_output : process(clk100, reset_n)
  begin
    if reset_n = '0' then 
      parallel_frame <= (others => '1');
    elsif rising_edge(clk100) then
      if enable_frame = '1' then
        parallel_frame <= full_frame;
      end if;
    end if;
end process p_output;

-- Combinatoire et process g�rant l'affichage sur les LEDs de la derni�re trame envoy�e
p_LEDs : process(clk100, reset_n)
  begin
    if reset_n = '0' then 
      LEDs <= (others => '0');
    elsif rising_edge(clk100) then
      if enable_frame = '1' then
        LEDs <= s_LEDs;
      end if;
    end if;
end process p_LEDs;

s_LEDs(1 downto 0)   <= function_type;
s_LEDs(9 downto 2)   <= "00"&function_select(5 downto 0) when speed_enable = '1'
                   else "000"&function_select(4 downto 0) when functions1_enable = '1'
                   else function_select(7 downto 0);
s_LEDs(11 downto 10) <= train_addr;

end RTL;
