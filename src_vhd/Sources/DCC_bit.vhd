library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;

entity DCC_bit is
          generic 
          (
            -- Param�tres de configuration
            counter_value : natural -- Param�tre fixant la demi-p�riode du bit g�n�r� en format DCC
          );
          port
		      (
		        -- Inputs
   		      clk100, clk1, reset_n : in std_logic;
		        go : in std_logic;
		        
		        -- Outputs
		        fin : out std_logic;
		        DCC : out std_logic
          );
end DCC_bit;

architecture RTL of DCC_bit is

-- Configuration (auto)
constant nb_bits : integer := integer(ceil(log2(real(counter_value))))+1; -- Nombre de bits du compteur
constant match_register : std_logic_vector(nb_bits-1 downto 0) := std_logic_vector(to_unsigned(counter_value-1, nb_bits));

-- Compteur
signal timer : std_logic_vector(nb_bits-1 downto 0);
signal last : std_logic;
signal run : std_logic;

-- Machine � �tats
type state is (IDLE, LOW_LEVEL, TRANSIENT_L, HIGH_LEVEL, TRANSIENT_H);
signal EP, EF: state;	

begin

-- Process de la machine � �tats
p_FSM : process(clk100, reset_n)
  begin
		if reset_n = '0' then EP <= IDLE;
		elsif rising_edge(clk100) then
			EP <= EF;
		end if;
end process;

p_state : process(EP, go, last)
	begin
		case (EP) is
		  
			when IDLE	=> if go = '1' then
			               EF <= LOW_LEVEL;
			               run <= '1';
			             else
			               EF <= IDLE;
			               run <= '0';  
			             end if;
			             DCC <= '0';
			             fin <= '0';
									
			when LOW_LEVEL	=> if last = '1' then
			                    EF <= TRANSIENT_L;
			                    run <= '0';
			                  else
			                    EF <= LOW_LEVEL;
			                    run <= '1';
			                  end if;
			                  DCC <= '0';
			                  fin <= '0';

      when TRANSIENT_L => if last = '0' then
                          EF <= HIGH_LEVEL;
                          run <= '1';
                        else
                          EF <= TRANSIENT_L;
                          run <= '0';
                        end if; 
                        DCC <= '0';
                        fin <= '0';

			when HIGH_LEVEL	=> if last = '1' then
			                     EF <= TRANSIENT_H;
			                     run <= '0';
			                   else
			                     EF <= HIGH_LEVEL;
			                     run <= '1';
			                   end if;
			                   DCC <= '1';
			                   fin <= '0';

      when TRANSIENT_H => if last = '0' then
                            EF <= IDLE;
                            DCC <= '0';
                            fin <= '1';
                          else
                            EF <= TRANSIENT_H;
                            DCC <= '1';
                            fin <= '0';
                          end if; 
                          run <= '0';
   
		end case;
end process;

-- Process et combinatoire g�rant le compteur
p_timer : process(clk1, reset_n)
  begin
    if reset_n = '0' then timer <= (others => '0');
    elsif rising_edge(clk1) then
      if run = '1' then
        timer <= timer + 1;
      else
        timer <= (others => '0');
      end if;
    end if;
end process;

last <= '1' when timer = match_register else '0';

end RTL;
