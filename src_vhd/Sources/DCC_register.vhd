library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity DCC_register is port
          (
		       -- Inputs
   		     clk100, reset_n : in std_logic;
		       
		       command        : in std_logic_vector(1 downto 0);  -- Permet de charger le registre (01) ou bien de s�rialiser son contenu (10)
		       parallel_frame : in std_logic_vector(50 downto 0); -- Entr�es parall�le du registre
		       
		       -- Outputs
		       serial_frame   : out std_logic;                    -- Sortie s�rie du registre
		       last_bit_frame : out std_logic                     -- Indique lorsque le dernier �lement du regitre est s�rialis� (ne fonctionne que pour une trame DCC)
          );
end DCC_register;

architecture RTL of DCC_register is

-- "FIFO" contenant les donn�es � s�rialiser en fonctionnement big indian
signal FIFO : std_logic_vector(50 downto 0);

begin

p_register : process(clk100, reset_n)
  begin
    if reset_n = '0' then
      FIFO <= (others => '1');
      serial_frame <= '0';    
    elsif rising_edge(clk100) then

      -- SAVE - chargement parall�le
      if command = "01" then
        FIFO <= parallel_frame;

      -- LOAD - s�rialisation du contenu
      elsif command = "10" then
        serial_frame <= FIFO(50);
        FIFO(50 downto 1) <= FIFO(49 downto 0);
        FIFO(0) <= '0';
      end if;
    end if;
end process p_register;

last_bit_frame <= '1' when FIFO = '0'&x"000000000000" else '0';

end RTL;
