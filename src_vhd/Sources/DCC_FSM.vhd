library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity DCC_FSM is port
          (
		       -- Inputs
            clk100, clk1, reset_n : in std_logic;
            
            serial_frame   : in std_logic;                 -- Bit courant de la trame
            last_bit_frame : in std_logic;                 -- Indique que le dernier bit de la trame est disponible sur le port serial_frame
		        fin_0, fin_1   : in std_logic;                 -- Indique que le dernier bit envoy� � bien �t� traduit
		        
           -- Outputs
            command    : out std_logic_vector(1 downto 0); -- Commande indiquant si on charge la trame le DCC_register ou bien si on lit la trame s�rialis�e
            go_0, go_1 : out std_logic                     -- Commande indiquant le lancement de la g�n�ration d'un motif 0/1 format DCC
          );
end DCC_FSM;

architecture RTL of DCC_FSM is

-- Compteur et param�tres
constant match_register : std_logic_vector(12 downto 0) := '1'&x"770"; -- 6000 cycles (de 0 � 5999) soit 6 ms avec une horloge de 1 MHz  

signal timer : std_logic_vector(12 downto 0);
signal last  : std_logic;
signal run   : std_logic;

-- Machine � �tats
type state is (IDLE, BURST, ONE_TRANSMIT, ZERO_TRANSMIT);
signal EP, EF: state;	

begin

-- Process de la machine � �tats g�rant le chagement d'�tats
p_FSM : process(clk100, reset_n)
  begin
		if reset_n = '0' then EP <= IDLE;
		elsif rising_edge(clk100) then
			EP <= EF;
		end if;
end process p_FSM;

-- Process de la machine � �tats g�nant le combinatoire
p_state : process(EP, last, fin_1, fin_0, serial_frame, last_bit_frame)
	begin
		case (EP) is
		  
			when IDLE	=> if last = '1' then -- Si le compteur atteind sa valeur
			               run <= '0';      -- On arr�te de compter
			               command <= "10"; -- On charge le prochain bit du registre DCC 
			               EF <= BURST;     -- On passe � l'�tat suivant
			             else
			               run <= '1';      -- Sinon on continue de compter
			               command <= "01"; -- On remplit la FIFO
			               EF <= IDLE;      -- On ne change pas d'�tat
			             end if;
			             go_1 <= '0';
			             go_0 <= '0';

			when BURST	=> if serial_frame = '1' then -- Si le bit de la trame utilisateur vaut 1
			                go_1 <= '1';             -- On demarre la g�n�ration d'un bit � un en format DCC
			                go_0 <= '0';
			                EF <= ONE_TRANSMIT;      -- On passe � l'un des �tats suivants
                    else                       -- Sinon, c'est que le bit utilisateur vaut 0   
                      go_1 <= '0';
                      go_0 <= '1';             -- On demarre la g�n�ration d'un bit � z�ro en format DCC
                      EF <= ZERO_TRANSMIT;     -- On passe � l'un des �tats suivants
                    end if;
                    command <= "00";
                    run <= '0'; 
                     
			when ONE_TRANSMIT	=> if (fin_1 = '1') and (last_bit_frame = '1') then    -- Si la g�n�ration du bit 1 en format DCC est termin�e et que c'est le dernier de la trame utilisateur
			                       command <= "00";
			                       EF <= IDLE;                                       -- On passe � l'�tat suivant
			                     elsif (fin_1 = '1') and (last_bit_frame = '0') then -- Si la g�n�ration du bit 1 en format DCC est termin�e et que ce n'est pas le derner
			                       command <= "10";                                  -- On charge le prochain bit du registre DCC 
			                       EF <= BURST;                                      -- On passe � l'�tat suivant
			                     else                                                -- Sinon, on attend
			                       command <= "00";
			                       EF <= ONE_TRANSMIT;                               -- On ne change pas d'�tat 
			                     end if;
			                     go_1 <= '0';
			                     go_0 <= '0';
			                     run <= '0'; 

      when ZERO_TRANSMIT => if fin_0 = '1' then    -- Si la g�n�ration du bit 0 en format DCC est termin�e
			                        command <= "10";     -- On charge le prochain bit du registre DCC 
			                        EF <= BURST;         -- On passe � l'�tat suivant
			                      else                   -- Sinon, on attend
			                        command <= "00";
			                        EF <= ZERO_TRANSMIT; -- On ne change pas d'�tat
			                      end if;
			                      go_0 <= '0';
			                      go_1 <= '0';
                            run <= '0'; 
      
		end case;
end process p_state;

-- Process et combinatoire g�rant le compteur
p_timer : process(clk1, reset_n)
  begin
    if reset_n = '0' then timer <= (others => '0');
    elsif rising_edge(clk1) then
      if run = '1' then                             -- On compte
        timer <= timer + 1;
      else                                          -- On arr�te de compter et le compteur se r�initialise
        timer <= (others => '0');
      end if;
    end if;
end process p_timer;

last <= '1' when timer = match_register else '0';

end RTL;
